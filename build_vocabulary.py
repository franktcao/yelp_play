'''Build the vocabulary for the yelp dataset'''

import sys
import os
import json

import pprint
import argparse
from collections import Counter
  

# Stop words are words that occur very frequently,
# and don't carry much information about the quality of the review. 
# We keep 'not' since negation is important.
# '!' is also kept since it tends to be more frequent in negative reviews
# and typically used to make a statement stronger (in good or in bad). 
stopwords = set(['.','i','a','and','the','to', 'was', 'it', 'of', 'for', 'in',
                 'my', 'that', 'so', 'do', 'our', 'the', 'and', ',', 'my', 'in',                 'we', 'you', 'are', 'is', 'be', 'me'])

def main():
    from vocabulary import Vocabulary # cbernet's class for vocabularies
    import parallelize

    args = parse_args()

    fnames = args.pattern
    datadir = args.datadir
    num_words = args.num_words
    
    nprocesses = len(fnames) if args.parallel else None
    results = parallelize.run(process_file, fnames, nprocesses, args)

    full_counter = Counter()
    for counter in results:
      full_counter.update(counter)

    vocabulary = Vocabulary(full_counter, n_most_common=num_words)
    vocabulary.save( datadir + 'index' )

    pprint.pprint(full_counter.most_common(10))
    print(len(full_counter))
    # Prints the default 20 words from the vocabulary
    print(vocabulary)

def process_file(fname, options):
    '''Process a review JSLON lines file and count the occurence 
    of each words in all reviews. Returns the counter, which will 
    be used to find the most frequent words.
    '''
    print(fname)
    with open(fname) as ifile:
        counter = Counter()
        for i,line in enumerate(ifile):
            if i==options.lines:
                break
            if i%10000==0:
                print(i)            
            data = json.loads(line) 
            # extract what we want
            words = data['text']               
            for word in words:
                if word in stopwords:
                    continue
                counter[word]+=1
        return counter

def parse_filepath(full_fpath):
    drive, fpath = os.path.splitdrive(full_fpath)
    path, fname = os.path.split(fpath)
    return fname, path, drive

def parse_args():
    '''Set up command line options.  '''

    parser = argparse.ArgumentParser(description='''Take split files 
        and tokenize the comments.''')
    parser.add_argument(dest="pattern", nargs='+',
        help="""Filename string pattern. 
        Pattern should match the files you want to process, e.g. 'xa?'""")
    parser.add_argument("-N", "--num_words",
        dest="num_words", default=20000, type=int,
        help="Max number of words in vocabulary (default: 20k).")
    parser.add_argument("-l", "--lines", dest="lines", 
        default=sys.maxsize, type=int,
        help="Max number of lines (default: all lines)")
    parser.add_argument("-p", "--parallel", dest="parallel", 
        action="store_true", default=True, 
        help="Parallel mode (default: False)")
    parser.add_argument("-d", "--datadir", dest="datadir", 
        default=os.path.expanduser('data/partitioned/tokenized/'),
        help="Data directory of where the split files are")
    
    if len(sys.argv) < 2:
      print('You need to have more at least one input file name')
      parser.print_help()
      sys.exit(1)

    args = parser.parse_args()
    
    return args


if __name__ == '__main__':
    main()



#paths = []
#fnames = []
#for fpath in full_fpaths:
#  fname, path, drive = parse_filepath(fpath)
#  fnames.append(fname)
#  paths.append(path)
#  print(fname, path, drive)


## Auld
#import glob # For expanding wildcard characters in string)
#olddir = os.getcwd()
#os.chdir(args.datadir)
##fnames = glob.glob(args.pattern)

#full_fpaths = args.pattern
