import sys 
import os

import json
import h5py

import numpy as np

def main():
    import parallelize

    args = parse_args()

    fnames = args.pattern
    datadir = args.datadir

    nprocesses = len(fnames) if args.parallel else None
    results = parallelize.run(get_array, fnames, nprocesses, args)
    write_array_to_file(results, datadir)

def count_lines(fname):
    '''Counts the number of lines in the file with name fname '''
    line_count_cmd = 'cat {} | wc -l'.format(fname)
    count = int(os.popen(line_count_cmd).read())
    return count

def get_array(fname, options):
    '''Converts encoded JSON file into a numpy array and returns it.  '''
    print('Getting np array from', fname)
    ifile = open(fname)
    
    # Limit the number of words in reviews
    max_lines = options.lines
    lines_in_file = count_lines(fname)
    nrows = min(lines_in_file, max_lines)
    
    # We reserve the first four columns/features for:
    # 1. number of stars, 
    # 2., 3., 4. whether or not they have useful, funny, cool attribute
    n_features = 4
    # Limit the number of words in reviews
    word_limit = options.num_words
    ncols =  n_features + word_limit
    
    # Initialized to zeros to do padding already
    all_data = np.zeros((nrows, ncols), dtype=np.int16)
    for i, line in enumerate(ifile): 
        if i%10000 == 0:
            print(i)        
        if i == max_lines:
            break        
        data = json.loads(line) 
        codes = data['text']  
        # We can decide to keep the unknown words (code=1) 
        # or just to drop them (default).
        drop_unknown = not options.keep_unknown
        if drop_unknown:
            codes = [code for code in codes if code != 1]
        # Store rating in the 1st column
        all_data[i,0] = data['stars']
        all_data[i,1] = data['useful']
        all_data[i,2] = data['funny']
        all_data[i,3] = data['cool']
        # Store the encoded words afterwards the review is truncated to limit.
        truncated = codes[:word_limit]
        all_data[i,n_features:n_features+len(truncated)] = truncated
    ifile.close()
    print(fname,  'done')
    return all_data

def write_array_to_file(results, datadir):
    # Concatenating the numpy arrays for all files
    print('concatenating')
    data = np.concatenate(results)
    print(data) 
    # Saving the full numpy array to an hdf5 file
    ofname = datadir+'encoded_review_array.h5'
    print('Writing array to {}/{}'.format(ofname,'reviews'))
    h5 = h5py.File(ofname, 'w')
    h5.create_dataset('reviews', data=data) 
    h5.close()    


def parse_args():
    import argparse
    '''Set up command line options.  '''

    parser = argparse.ArgumentParser(description='''Take encoded files 
        and fill numpy arrays with them.''')
    parser.add_argument(dest="pattern", nargs='+',
        help="""Filename string pattern. 
        Pattern should match the files you want to process, e.g. 'xa?'""")
    parser.add_argument("-p", "--parallel", dest="parallel", 
        action="store_true", default=True, 
        help="Parallel mode (default: False)")
    parser.add_argument("-d", "--datadir", dest="datadir", 
        default=os.path.expanduser('data/partitioned/tokenized/encoded/'),
        help="Data directory of where the split files are")
    parser.add_argument("-l", "--lines", dest="lines", 
        default=sys.maxsize, type=int,
        help="Max number of lines (default: all lines)")
    parser.add_argument("-N", "--num_words",
        dest="num_words", default=20000, type=int,
        help="Max number of words in vocabulary (default: 20k).")
    parser.add_argument("-u", "--keep-unknown", dest="keep_unknown", 
        action="store_true", default=False, 
        help="Keep words unknown to vocabulary mode (default: False)")
    
    if len(sys.argv) < 2:
      print('You need to have more at least one input file name')
      parser.print_help()
      sys.exit(1)

    args = parser.parse_args()
    print(args.keep_unknown)
    
    return args
    
if __name__ == '__main__':
    main()
