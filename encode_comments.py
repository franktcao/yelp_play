import sys
import os

import json
from collections import Counter
import argparse

def main():
    from vocabulary import Vocabulary
    import parallelize
    
    args = parse_args()

    fnames = args.pattern 
    datadir = args.datadir
    print('Encoding comments for files: ', fnames)
    vocabulary = Vocabulary.load(datadir+'index')

    nprocesses = len(fnames) if args.parallel else None
    results = parallelize.run(encode_comments, fnames, nprocesses, 
                              args, vocabulary)

def encode_comments(fname, options, vocabulary):
    '''Process a review JSON lines file and count the words in all reviews.
    Returns the counter which will be used to find the most frequent words '''
    ofname = output_fname(fname)  
    print('Opening', fname)
    print('Writing', ofname)
    ifile = open(fname) 
    ofile = open(ofname,'w')
    for i,line in enumerate(ifile): 
        if i==options.lines:
            break
        if i%10000==0:
            print(i)        
        data = json.loads(line) 
        words = data['text']     
        codes = vocabulary.encode(words)
        data['text'] = codes
        line = json.dumps(data)
        ofile.write(line+'\n')        
    ifile.close()
    ofile.close()
    
def parse_filepath(full_fpath):
    ''' Splits full path name 'C://path/to/file.ext' 
        to 'C://', /path/to', 'file', '.ext' '''
    drive, fpath = os.path.splitdrive(full_fpath)
    path, fname = os.path.split(fpath)
    # Split extension and name 'file.ext' to 'file', '.ext'
    fname, ext = os.path.splitext(fname)
    return drive, path, fname, ext 

def output_fname(input_fname):
    _, path, fname, ext = parse_filepath(input_fname)
    path += '/encoded/'
    fname = path + fname 
    fname += '_enc'
    fname += ext
    #print(fname)
    #sys.exit()
    return fname 

def parse_args():
    '''Set up command line options.  '''

    parser = argparse.ArgumentParser(description='''Take split files 
        and tokenize the comments.''')
    parser.add_argument(dest="pattern", nargs='+',
        help="""Filename string pattern. 
        Pattern should match the files you want to process""")
    parser.add_argument("-N", "--num_words",
        dest="num_words", default=20000, type=int,
        help="Max number of words in vocabulary (default: 20k).")
    parser.add_argument("-l", "--lines", dest="lines", 
        default=sys.maxsize, type=int,
        help="Max number of lines (default: all lines)")
    parser.add_argument("-p", "--parallel", dest="parallel", 
        action="store_true", default=False, 
        help="Parallel mode (default: False)")
    parser.add_argument("-d", "--datadir", dest="datadir", 
        default=os.path.expanduser('data/partitioned/tokenized/'),
        help="Data directory of where the split files are")
    
    if len(sys.argv) < 2:
      print('You need to have more at least one input file name')
      parser.print_help()
      sys.exit(1)

    args = parser.parse_args()
    
    return args

if __name__ == '__main__':
    main()
