'''Tokenize a JSON lines dataset with nltk '''
import os 
import sys 

import argparse
import json

import nltk
# to be done only once:
#nltk.download('punkt')


def main():
    import parallelize
    
    args = parse_args()
    
    if args.motivation:
        motivation()
        sys.exit()

    fnames = args.pattern
    
    nprocesses = len(fnames) if args.parallel else None
    
    results = parallelize.run(process_file, fnames, nprocesses, args)
            

def process_file(fname, options):
    '''Tokenize data in file fname. The output is written to fname_tok.json '''
    ofname = output_fname(fname)
    print('Opening', fname)
    print('Writing to', ofname)
    ifile = open(fname)
    ofile = open(ofname,'w')
    for i, line in enumerate(ifile):
        if i%1000 == 0:
            print(i) 
        if i==options.lines:
            break        
        # Convert the json line to dict
        data = json.loads(line) 
        # Extract relevant review text
        text = data['text']
        # Tokenize with NLTK
        words = nltk.word_tokenize(text)
        # Convert all words to lower case 
        words = [word.lower() for word in words]
        # Update JSON and write to output file
        data['text'] = words
        line = json.dumps(data)
        ofile.write(line+'\n')
    ifile.close()
    ofile.close()

def motivation():
    # Motivation for tokenizing with nltk
    review = '''I went there for a hair cut. Hair wash and stylist was great, 
                but it was very hard to communicate with them since they all 
                spoke chinese and not so good English. The stylist didn't quite 
                understand me while the outcome was not bad. \n\nThe website 
                said $50 for senior stylist but they charged me $60 + tax. 
                Cash only, so I wonder why they charge tax?? Including tip, 
                I ended up paying  $80 just for simple trimming.'''

    # Naiively splitting words in a string
    print('Naive tokenization', review.split())

    # Using NLTK package to split words in a string
    print('Tokenization with nltk', nltk.word_tokenize(review))


def output_fname(input_fname):
    #part_fname0 = os.path.splitext(input_fname)[0]
    fname, path, _ = parse_filepath(input_fname)
    path += '/tokenized/'
    fname = path + fname 
    fname += '_tok.json'
    return fname 

def parse_filepath(full_fpath):
    drive, fpath = os.path.splitdrive(full_fpath)
    path, fname = os.path.split(fpath)
    return fname, path, drive

def parse_args():
    '''Set up command line options.  '''

    parser = argparse.ArgumentParser(description='''Take split files and 
        tokenize the comments.''')
    parser.add_argument(dest="pattern", nargs='+',
        help="""Filename string pattern. 
        Pattern should match the files you want to process, e.g. 'xa?'""")
    #parser.add_argument(dest="pattern", 
    #    help="""Filename string pattern. Pattern 
    #            should match the files you want to process, 
    #            e.g. 'xa?'""")
    parser.add_argument("-l", "--lines", dest="lines", 
        default=sys.maxsize, type=int,
        help="Max number of lines (default: all lines)")
    parser.add_argument("-m", "--motivation", dest="motivation", default=False, 
        help="Print out the motivation and exit if True (default: False)")
    parser.add_argument("-p", "--parallel", dest="parallel", 
        action="store_true", default=True, 
        help="Parallel mode (default: False)")
    parser.add_argument("-d", "--datadir", dest="datadir", 
        default=os.path.expanduser('data/partitioned/'),
        help="Data directory of where the split files are")

    args = parser.parse_args()
    return args


    
if __name__ == '__main__':
    main()

#import glob
#olddir = os.getcwd()    
#os.chdir(args.datadir)
#fnames = glob.glob(args.pattern)
