#! /bin/bash

# Takes full path to a text file "FULLPATH" and splits it into 
# "N_LINES_PER_FILE" in current working directory

FULLPATH=$1
N_LINES_PER_FILE=$2

# Reverse, and get the first element from the cut then reverse back
FILE=$(echo $FULLPATH | rev | cut -d'/' -f 1 | rev )
# Reverse, and get elements after the second one on from the cut then reverse back
RELDIR=$(echo $FULLPATH | rev | cut -d'/' -f 2- | rev )/

split -l $N_LINES_PER_FILE $FULLPATH

# Take a look at one of the files
head -n 1 xab



#echo $FILE
#echo $RELDIR
#echo $FULLPATH
#RELDIR=$(echo $FULLPATH | rev | cut -d'.' -f 2 | rev )
#echo $RELDIR
#RELDIR=$(echo $RELDIR | cut -d'/' -f 1 )/
#RELDIR=$(echo $FULLPATH | cut -d'.' -f 1 )
#NAME=$(echo $FILE | cut -d'.' -f 1)
