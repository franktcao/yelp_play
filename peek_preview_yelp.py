import json
import pandas as pd
import matplotlib.pyplot as plt


def main():
    # Take a look at basic properties of the yelp dataset
    # Input file:
    #ifile = 'data/review.json'


    # Number of reviews you want to pull into memory
    n_reviews = 10000

    # The data as a python list
    data = json_to_list(ifile, n_reviews)

    # Create DataFrame with data extracted from loop
    df = pd.DataFrame(data, columns=['stars','text'])

    # Save formatted df to h5 file
    #df.to_hdf('revie20ws.h5','reviews')

    # Take a look at differnt columns
    stars = df['stars']
    text  = df['text']
    sorted(stars.unique())
    print(df.iloc[1322].text)

    # Plot histogram of the stars
    plt.hist(stars, range=(0.5, 5.5), bins=5)
    plt.title('star distribution')
    plt.xlabel('stars')
    plt.ylabel('count')
    #plt.axvline(x=minloc, color = 'r', linewidth = 1, linestyle = '--')
    draw_temp_plot()

    # Plot histogram of the character count 
    plt.clf() # clear previous figure
    plt.hist(text.str.len(), bins=100)
    plt.title('character count distribution')
    plt.xlabel('number of characters')
    plt.ylabel('count')
    draw_temp_plot()




def json_to_list(fname, nlines):
    # Returns a list of tuples where each element is a row of the data
    
    all_data = list()
    ifile = open(fname)
    
    # Loop over the lines in the file and put them into a pandas dataframe
    for i, line in enumerate(ifile):
        if i == nlines:
            break

        # Convert .json entry to a dict
        data = json.loads(line)

        # Extract what we want
        text  = data['text']
        stars = data['stars']
        
        # Add to the data collected so far
        all_data.append([stars, text])
    ifile.close()
    return all_data

def draw_temp_plot():
    plt.grid()
    plt.show(block=False)
    input("Hit Enter To Close")
    plt.close()


if __name__ == '__main__':
    main()
